import base.Medicamento;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

public class Vista {

    private JFrame frame;
    private JPanel panel1;
    private JTextField nMedicamento;
    private JComboBox cBox;
    private JButton aMedicamentosBtn;
    private JButton mMedicamentosBtn;
    private JTextField tMedicamento;
    private JLabel labelMed;

    private LinkedList<Medicamento> lista;
    private DefaultComboBoxModel<Medicamento> dcbm;

    public Vista() {
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setSize(800, 600);

        crearMenu();
        frame.setLocationRelativeTo(null);
        lista = new LinkedList<>();
        dcbm = new DefaultComboBoxModel<>();
        cBox.setModel(dcbm);

        aMedicamentosBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Dar de alta un medicamento
                altaMedicamento(nMedicamento.getText(), tMedicamento.getText());
                //Listar medicamentos en la lista del combo
                refrescarComboBox();
            }
        });

        mMedicamentosBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Muestra el medicamento seleccionado
                Medicamento medicamento = (Medicamento) dcbm.getSelectedItem();
                labelMed.setText(medicamento.toString());
            }
        });
    }

    private void refrescarComboBox() {
        dcbm.removeAllElements();
        for (Medicamento medicamento : lista) {
            dcbm.addElement(medicamento);
        }
    }

    public static void main(String[] args) {
        Vista vista = new Vista();
    }

    private void altaMedicamento(String nombre, String tipo){
        lista.add(new Medicamento(nombre, tipo));
    }

    private void crearMenu() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        JMenuItem itemExportarXML = new JMenuItem("Exportar XML");
        JMenuItem itemImportarXML = new JMenuItem("Importar XML");

        itemExportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcionSeleccionada = selectorArchivo.showSaveDialog(null);
                if(opcionSeleccionada == JFileChooser.APPROVE_OPTION){
                    File fichero = selectorArchivo.getSelectedFile();
                    exportarXML(fichero);
                }
            }
        });

        itemImportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcion = selectorArchivo.showOpenDialog(null);
                if(opcion == JFileChooser.APPROVE_OPTION){
                    File fichero = selectorArchivo.getSelectedFile();
                    importarXML(fichero);
                    refrescarComboBox();
                }
            }
        });

        menu.add(itemExportarXML);
        menu.add(itemImportarXML);

        barra.add(menu);
        frame.setJMenuBar(barra);
    }


    private void importarXML(File fichero) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document documento = builder.parse(fichero);

            //Recorro cada uno de los nodos medicamento para obtener sus campos
            NodeList medicamentos = documento.getElementsByTagName("medicamento");
            for (int i = 0; i < medicamentos.getLength(); i++) {
                Node medicamento = medicamentos.item(i);
                Element elemento = (Element) medicamento;

                //Obtengo los campos nombre y tipo
                String nombre = elemento.getElementsByTagName("nombre").item(0).getChildNodes().item(0).getNodeValue();
                String tipo = elemento.getElementsByTagName("tipo").item(0).getChildNodes().item(0).getNodeValue();

                altaMedicamento(nombre, tipo);
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }

    private void exportarXML(File fichero) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;

        try {
            builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();

            //Creo documento que representa arbol XML
            Document documento = dom.createDocument(null, "xml", null);

            //Creo el nodo raiz (medicamento) y lo añado al documento
            Element raiz = documento.createElement("medicamentos");
            documento.getDocumentElement().appendChild(raiz);

            Element nodoMedicamento;
            Element nodoDatos;
            Text dato;

            //Por cada coche de la lista, creo un nodo coche
            for (Medicamento medicamento : lista) {

                //Creo un nodo coche y lo añado al nodo raiz (coches)
                nodoMedicamento = documento.createElement("medicamento");
                raiz.appendChild(nodoMedicamento);

                //A cada nodo coche le añado los nodos marca y modelo
                nodoDatos = documento.createElement("nombre");
                nodoMedicamento.appendChild(nodoDatos);

                //A cada nodo de datos le añado el dato
                dato = documento.createTextNode(medicamento.getNombre());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("tipo");
                nodoMedicamento.appendChild(nodoDatos);

                dato = documento.createTextNode(medicamento.getTipo());
                nodoDatos.appendChild(dato);
        }
            //Transformo el documento anterior en un ficho de texto plano
            Source src = new DOMSource(documento);
            Result result = new StreamResult(fichero);

            Transformer transformer = null;
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(src, result);

        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }
}
